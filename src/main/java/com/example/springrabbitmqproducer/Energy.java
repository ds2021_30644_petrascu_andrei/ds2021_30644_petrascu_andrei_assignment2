package com.example.springrabbitmqproducer;

import com.sun.xml.internal.ws.developer.Serialization;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Energy {
    private UUID id;
    private Date timestamp;
    private Integer consumption;
    private UUID sensorId;
}
