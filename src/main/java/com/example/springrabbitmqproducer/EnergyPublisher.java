package com.example.springrabbitmqproducer;

import com.opencsv.CSVReader;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/publish")
//@EnableScheduling
public class EnergyPublisher {

    @Autowired
    private RabbitTemplate template;

    private List<Integer> values = getValuesFromCSV();
    private Date currentDate = new Date();

    //@Scheduled(fixedDelay = 1000)
    @PostMapping("/{id}")
    public String publishEnergy(@PathVariable("id") UUID id) {

        for (int index = 0; index < 10; index++) {
            System.out.println(" running " + index);
            Energy energy = new Energy();
            energy.setId(UUID.randomUUID());
            energy.setConsumption(values.get(index));
            energy.setTimestamp(currentDate);
            //energy.setSensorId(UUID.fromString("51108c19-ae9d-482a-aa97-9a4a77097aa1"));
            energy.setSensorId(id);
            template.convertAndSend(MQConfig.EXCHANGE, MQConfig.ROUTING_KEY, energy);
            currentDate = DateUtils.addMinutes(currentDate, 10);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return "Energies published!";
    }

    public List<Integer> getValuesFromCSV() {
        try (CSVReader reader = new CSVReader(new FileReader("sensor.csv"))) {
            List<String[]> r = reader.readAll();
            List<Integer> values = new ArrayList<>();
            r.forEach(x -> values.add(Math.round(Float.parseFloat(x[0]))));
            return values;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
